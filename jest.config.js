module.exports = {
  setupFiles: ['./jest.setup.ts'],
  globals: {
    'ts-jest': {
      tsConfig: 'tsconfig.json'
    },
    '__API__': '',
    window: true,
  },
  moduleFileExtensions: ['ts','js'],
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest'
  },
  testMatch: [
    '**/*.spec.ts'
  ],
  moduleNameMapper: {
    '^@app/(.*)$': '<rootDir>/src/app/$1',
    '^@server/(.*)$': '<rootDir>/src/server/$1',
    '^@shared/(.*)$': '<rootDir>/src/shared/$1',
    '@data/beers.json': "<rootDir>/src/data/beers.json"
  },
  testEnvironment: 'jest-environment-jsdom-global'
};
