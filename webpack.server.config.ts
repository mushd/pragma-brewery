import path from 'path';
import webpack from 'webpack';
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin';
import NodemonPlugin from 'nodemon-webpack-plugin';
import nodeExternals from 'webpack-node-externals';

const config: webpack.Configuration =  {
  target: 'node',
  mode: 'development',
  externals: [nodeExternals()],
  entry: './src/server/main.ts',
  output: {
    filename: 'server.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.s(c|a)ss$/,
        loader: [
          'style-loader',
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          }
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.ts', '.js', '.scss'],
    plugins: [
      new TsconfigPathsPlugin()
    ]
  },
  stats: {
    colors: true
  },
  devtool: 'source-map',
  plugins: [
    new NodemonPlugin({
      watch: path.resolve('./dist/server.js'),
      ignore: ['*.js.map'],
      verbose: true,
      script: './dist/server.js',
      ext: 'js,njk,json'
    }), // Dong
  ]
};

export default config;
