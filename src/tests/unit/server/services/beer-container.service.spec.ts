import { BeerContainerService } from '@server/services/beer-container.service';
import { Container } from 'typedi';
import { ContainerRepository } from '@server/repositories/container.repository';
import { TemperatureChange } from '@shared/models';
import { ChangeTypes } from '@shared/enums/change-type.enum';
import { SocketServerService } from '@server/services/socket-server.service';
import { TemperatureChangeRepository } from '@server/repositories/temperature-change.repository';

describe('BeerContainerService', () => {

  const id = 2;
  let beerContainerService: BeerContainerService = Container.get(BeerContainerService);
  let beerRepository: ContainerRepository = Container.get(ContainerRepository);
  let socketServerService: SocketServerService = Container.get(SocketServerService);
  let temperatureChangeRepository: TemperatureChangeRepository = Container.get(TemperatureChangeRepository);

  it('should ', async (done) => {

    const beer = await beerRepository.get(id);
    const amount = 1;
    const test = beer.temperature.current + amount;
    const type = ChangeTypes.Manual;

    const change: TemperatureChange = {
      id,
      amount,
      type
    };

    jest.spyOn(socketServerService, 'emitChange').mockReturnValue();
    jest.spyOn(socketServerService, 'emitAlert').mockReturnValue();

    const expected = await beerContainerService.changeTemperature(id, change);
    const changeList = await temperatureChangeRepository.list();
    expect(expected.temperature.current).toEqual(test);
    expect(changeList.length).toEqual(1);
    done();
  });
});
