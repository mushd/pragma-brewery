import { BaseApiService } from '@shared/utils/base-api.service';
import axios from  'axios';
import MockAdapter from 'axios-mock-adapter';
import { BaseModel } from '@shared/models/base.model';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

interface MockModel extends BaseModel {
  name: string;
}

class MockApiService extends BaseApiService<MockModel> {
  constructor() {
    super('mock-url');
  }
}

describe('BaseApiService', () => {

  let service: MockApiService;
  const mock = new MockAdapter(axios);
  const url = 'mock-url';

  beforeEach(() => {
    service = new MockApiService();
  });

  describe('get', () => {

    const id  = 1;
    const urlGet = `/${url}/${id}`;
    const data: MockModel = {
      id,
      name: 'item 1'
    };
    beforeEach(() => {});

    it('should return a list', (done) => {

      mock.onGet(urlGet).reply(200, data);
      const axiosGet = jest.spyOn(axios, 'get');

      service.get(id)
        .subscribe((data) =>{
          expect(axiosGet).toHaveBeenCalledWith(urlGet);
          expect(data).toEqual(data);
          done();
        });
    });

    it('should return error', (done) => {
      mock.onGet(urlGet).reply(400);
      const axiosGet = jest.spyOn(axios, 'get');
      service.get(id)
        .pipe(
          catchError(error => {
            expect(axiosGet).toHaveBeenCalledWith(urlGet);
            expect(error.message).toEqual('Request failed with status code 400');
            done();
            return of();
          })
        )
        .subscribe(() =>{});
    }, 5000);
  });
});
