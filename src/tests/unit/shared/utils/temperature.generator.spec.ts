import { Container } from 'typedi';
import { timer } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { TemperatureGenerator } from '@shared/utils/temperature.generator';
import { TemperatureChange } from '@shared/models/temperature-change.model';

describe('Temperature Service', () => {

  let temperatureService: TemperatureGenerator;
  beforeEach(() => {
    temperatureService = Container.get(TemperatureGenerator);
  });

  it('should emit random containers changes', (done) => {

    const changes: TemperatureChange[] = [];
    let next = 0;

    temperatureService.temperatureChanges()
      .pipe(
        tap((change) => changes.push(change)),
        takeUntil(timer(10000))
      )
      .subscribe({
        next: (change) => {
          next++;
          expect(change.id).toBeDefined();
          expect(change.amount).toBeDefined();
        },
        complete: () => {
          expect(changes.length).toBe(next);
          done();
        },
      });
  }, 10010);
});
