import { BaseRepository } from '@shared/utils/base.repository';
import { BaseModel } from '@shared/models/base.model';

interface MockModel extends BaseModel {
  name: string;
}

const mockData: Map<number, MockModel> = new Map([
  [1, {id: 1, name: 'Item 1'}],
  [2, {id: 2, name: 'Item 2'}],
  [3, {id: 3, name: 'Item 3'}],
  [4, {id: 4, name: 'Item 4'}],
  [5, {id: 5, name: 'Item 5'}],
  [6, {id: 6, name: 'Item 6'}],
  [7, {id: 7, name: 'Item 7'}],
  [8, {id: 8, name: 'Item 8'}],
]);

class MockRepository extends BaseRepository<MockModel> {
  constructor() {
    super(mockData);
  }
}

describe('BaseRepository', () => {

  let baseRepository: MockRepository = new MockRepository();

  beforeEach(() =>{
    baseRepository = new MockRepository();
  });

  it('should get one item', async () => {
    const id = 2;
    const item = await baseRepository.get(id);

    expect(item).toEqual(mockData.get(id));
  });

  it('should create one item', async () => {
    const newItem: MockModel = {
      name: 'Item'
    };

    const created = await baseRepository.create(newItem);

    expect(created.id).not.toBeUndefined();
    expect(created.name).toEqual(newItem.name);
  });

  it('should update one item', async () => {
    const id = 2;
    const name = 'change name';
    let item = await baseRepository.get(id);

    item.name = name;
    const updated = await baseRepository.update(id, item);

    item = await baseRepository.get(id);

    expect(updated.name).toEqual(name);
    expect(updated.id).toEqual(item.id);
  });

});
