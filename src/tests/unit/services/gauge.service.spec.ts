import { GaugeService } from '@app/services/gauge.service';
import { Container } from 'typedi';
import { takeUntil } from 'rxjs/operators';
import { timer } from 'rxjs';
import { BeerContainer } from '@shared/models';
import { ChangeTypes } from '@shared/enums/change-type.enum';

describe('Gauge Service', () => {

  let gaugeService: GaugeService;
  let gauge: HTMLCanvasElement;

  beforeEach(() => {
    gaugeService = Container.get(GaugeService);
    const cont: BeerContainer = { // initial container containers 6
      id: 1,
      beer: 'Beer Test',
      temperature: {
        min: 4,
        max: 8,
        current: 6
      }
    };
    gauge = gaugeService.createForBeer(cont);
  });

  it('should emit an alert every time a container changes containers goes bellow the range', (done) => {

    expect(gauge).toBeInstanceOf(HTMLCanvasElement);

    gaugeService.temperatureAlerts()
      .subscribe(alert => {
        expect(alert).toEqual(0);
        expect(gauge).toBeInstanceOf(HTMLCanvasElement);
        done();
      });

    gaugeService.changeTemperature({id: 0, amount: -1, type: ChangeTypes.Manual}); //will not generate alert;
    gaugeService.changeTemperature({id: 0, amount: -1, type: ChangeTypes.Manual}); //will not generate alert;
    gaugeService.changeTemperature({id: 0, amount: -1, type: ChangeTypes.Manual}); //should generate alert;
  });

  it('should emit an alert every time a container changes containers goes above the range', (done) => {

    expect(gauge).toBeInstanceOf(HTMLCanvasElement);

    gaugeService.temperatureAlerts()
      .subscribe(alert => {
        expect(alert).toEqual(0);
        expect(gauge).toBeInstanceOf(HTMLCanvasElement);
        done();
      });

    gaugeService.changeTemperature({id: 0, amount: 1, type: ChangeTypes.Manual}); //will not generate alert;
    gaugeService.changeTemperature({id: 0, amount: 1, type: ChangeTypes.Manual}); //will not generate alert;
    gaugeService.changeTemperature({id: 0, amount: 1, type: ChangeTypes.Manual}); //should generate alert;
  });

  it('should not emit an alert when the gauge is already on alert state', (done) => {
    expect(gauge).toBeInstanceOf(HTMLCanvasElement);

    let emits = 0;
    gaugeService.temperatureAlerts()
      .pipe(takeUntil(timer(3000)))
      .subscribe({
        next: () => {
          emits++;
        },
        complete: () => {
          expect(emits).toBe(2);
          done();
        },
      });

    gaugeService.changeTemperature({id: 0, amount: 1, type: ChangeTypes.Manual}); //will not generate alert;
    gaugeService.changeTemperature({id: 0, amount: 1, type: ChangeTypes.Manual}); //will not generate alert;
    gaugeService.changeTemperature({id: 0, amount: 1, type: ChangeTypes.Manual}); //should generate alert;
    gaugeService.changeTemperature({id: 0, amount: -1, type: ChangeTypes.Manual}); //will not generate alert;
    gaugeService.changeTemperature({id: 0, amount: -1, type: ChangeTypes.Manual}); //will not generate alert;
    gaugeService.changeTemperature({id: 0, amount: 1, type: ChangeTypes.Manual}); //will not generate alert;
    gaugeService.changeTemperature({id: 0, amount: 1, type: ChangeTypes.Manual}); //will not generate alert;
    gaugeService.changeTemperature({id: 0, amount: 1, type: ChangeTypes.Manual}); //should generate alert;
    gaugeService.changeTemperature({id: 0, amount: 1, type: ChangeTypes.Manual}); //will not generate alert;
    gaugeService.changeTemperature({id: 0, amount: 1, type: ChangeTypes.Manual}); //will not generate alert;
  }, 4000);
});
