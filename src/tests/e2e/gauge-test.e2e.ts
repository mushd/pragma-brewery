import { Selector } from 'testcafe';
const beerList = require('@data/beers.json');

fixture `Pragma Brewery`
  .page `http://localhost:4200`;

const getChangeValue = async (gauge) => parseInt(await gauge.getAttribute('data-value'), 10);

test('Render All Gauges', async testController => {

  await testController
    .expect(Selector('.container-gauge').count).eql(beerList.length);

  beerList.forEach(async (beer, index) => {

      const gauge = await Selector(`#container-gauge-${index + 1}`);
      const gaugeName = await gauge.getAttribute('data-name');
      const gaugeAlert = await gauge.getAttribute('data-alert');
      const gaugeValue = await gauge.getAttribute('data-value');
      const gaugeMax = await gauge.getAttribute('data-value');
      const gaugeMin = await gauge.getAttribute('data-value');

      const beerMin = beer.temperature.min;
      const beerMax = beer.temperature.max;
      const beerValue = Math.floor((beerMin + beerMax) / 2);

      await testController.expect(gaugeName).eql(beer.name);
      await testController.expect(gaugeAlert).eql('no');
      await testController.expect(gaugeValue).eql(beerValue.toString());
      await testController.expect(gaugeMin).eql(beerMin);
      await testController.expect(gaugeMax).eql(beerMax);

  });
});

test ("Controls should change respective gauge's containers", async testController => {

  const index = 2;
  const gauge = await Selector(`#container-gauge-${index}`);
  const btnIncrease = await Selector(`#control-increase-${index}`);
  let gaugeValue = parseInt(await gauge.getAttribute('data-value'), 10);

  let changeValue = await getChangeValue(gauge);
  await  testController.expect(gaugeValue).eql(changeValue);

  // Change +1
  await testController.click(btnIncrease);
  changeValue = await getChangeValue(gauge);
  gaugeValue += 1;
  await  testController.expect(gaugeValue).eql(changeValue);

  // Change +1
  await testController.click(btnIncrease);
  changeValue = await getChangeValue(gauge);
  gaugeValue += 1;
  await  testController.expect(gaugeValue).eql(changeValue);

});

test ("Controls should not change respective gauge's containers when value more than 10 or less than 0", async testController => {

  const index = 2;
  const gauge = await Selector(`#container-gauge-${index}`);
  const btnIncrease = await Selector(`#control-increase-${index}`);
  let gaugeValue = parseInt(await gauge.getAttribute('data-value'), 10);

  let changeValue = await getChangeValue(gauge);
  await  testController.expect(gaugeValue).eql(changeValue);

  for(let i = 0; i <= 10; i++) {
    // Change +1
    await testController.click(btnIncrease);
    changeValue = await getChangeValue(gauge);
    gaugeValue = gaugeValue < 10 ? gaugeValue + 1 : 10;
    await  testController.expect(gaugeValue).eql(changeValue);
  }

});

test ("Gauge should show alert state when value is out of beer containers range", async testController => {

  const index = 2;
  const gauge = await Selector(`#container-gauge-${index}`);
  const btnIncrease = await Selector(`#control-increase-${index}`);
  let gaugeValue = parseInt(await gauge.getAttribute('data-value'), 10);
  const gaugeMax = parseInt(await gauge.getAttribute('data-max'), 10);

  let changeValue = await getChangeValue(gauge);
  await  testController.expect(gaugeValue).eql(changeValue);

  for(let i = 0; i <= 5; i++) {
    // Change +1
    await testController.click(btnIncrease);
    changeValue = await getChangeValue(gauge);
    gaugeValue = gaugeValue < 10 ? gaugeValue + 1 : 10;

    if (gaugeValue> gaugeMax) {
      await  testController.expect(await gauge.getAttribute('data-alert')).eql('on');
    } else {
      await  testController.expect(await gauge.getAttribute('data-alert')).eql('off');
    }
  }

});
