import { Service } from 'typedi';
import { ReplaySubject } from 'rxjs';
import { TemperatureChange } from '@shared/models';
import { SocketMessages } from '@shared/enums/socket-messages.enum';
const socket = require('socket.io-client/dist/socket.io');

@Service()
export class SocketClientService {

  private _changes: ReplaySubject<TemperatureChange> = new ReplaySubject();
  private _client;

  public changes(): ReplaySubject<TemperatureChange> {
    return this._changes;
  }

  start(): void {
    this._client = socket(__API__);

    this._client.on(SocketMessages.TemperatureChange, (change)  => {
      this._changes.next(JSON.parse(change));
    });
  }
}
