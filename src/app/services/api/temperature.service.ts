import { Service } from 'typedi';
import { BaseApiService } from '@shared/utils/base-api.service';
import { TemperatureChange } from '@shared/models';
import { Observable } from 'rxjs';

@Service()
export class TemperatureService extends BaseApiService<TemperatureChange> {

  constructor() {
    super('temperature');
  }

  changeTemperature(change: TemperatureChange): Observable<TemperatureChange> {
    return this.put(change.id, change);
  }
}
