import { Service } from 'typedi';
import { BaseApiService } from '@shared/utils/base-api.service';
import { BeerContainer } from '@shared/models';

/**
 * Beer Service
 */
@Service()
export class ContainerService extends BaseApiService<BeerContainer> {

  constructor() {
    super('containers');
  }
}
