import { Service } from 'typedi';
import { ReplaySubject } from 'rxjs';
import { TemperatureChange } from '@shared/models/temperature-change.model';
import { ChangeTypes } from '@shared/enums/change-type.enum';

/**
 * Control Service
 */
@Service()
export class ControlService {

  private _controlEvents: ReplaySubject<TemperatureChange> = new ReplaySubject();
  private _gaugesDisplay = document.getElementById('gauges-display');

  /**
   * Return the controls events subject with containers changes.
   * @return {ReplaySubject<TemperatureChange>} the gauge canvas.
   */
  public controlEvents(): ReplaySubject<TemperatureChange> {
    return this._controlEvents;
  }

  /**
   * Creates the dom element for a gauge
   * @param {HTMLCanvasElement} gaugeCanvas - The gauge canvas.
   * @param {number} gaugeIndex - The gauge index.
   * @return {HTMLElement} the gauge canvas.
   */
  public appendGauge(gaugeCanvas: HTMLCanvasElement, gaugeIndex: number): HTMLElement {
    const display = this.createDisplay(gaugeIndex);
    display.append(gaugeCanvas);
    display.append(this.createControlBox(gaugeIndex));
    return this.appendToScreen(display);
  };

  /**
   * Appends the gauge to the dom
   * @param {HTMLElement} display - The dom element.
   * @return {HTMLElement} the gauge canvas.
   */
  protected appendToScreen(display: HTMLElement): HTMLElement {
    this._gaugesDisplay.append(display);
    return display;
  }

  /**
   * Creates a dom element for a gauge
   * @param {number} index - The gauge index.
   * @return {HTMLElement} the dom element.
   */
  private createDisplay(index: number): HTMLElement {
    const display = document.createElement('section');
    display.id = `gauge-${index + 1}`;
    display.className = 'flex flex-col padding-10';
    return display;
  }

  /**
   * Creates a control box element for a gauge
   * @param {number} index - The gauge index.
   * @return {HTMLElement} the control box dom element.
   */
  private createControlBox(index: number): HTMLElement {
    const controlBox = document.createElement('section');
    controlBox.className = 'flex align-center';
    controlBox.append(this.createControl(index, -1));
    controlBox.append(this.createControl(index, +1));
    return controlBox;
  }

  /**
   * Creates a control box element for a gauge
   * @param {number} index - The gauge index.
   * @param {number} amount - The control containers amount.
   * @return {HTMLElement} the control box dom element.
   */
  protected createControl(id: number, amount: number): HTMLElement {
    const control = document.createElement('button');
    control.id = `control-${amount > 0 ? 'increase' : 'decrease'}-${id + 1}`;
    control.className = 'margin-5 padding-10 font-16 font-bold width-50 border-rad-20 txt-dark border-dark no-outline';
    control.innerHTML = `${amount > 0 ? '+1' : amount.toString()}°`;
    const type = ChangeTypes.Manual;
    control.addEventListener('click', () => this._controlEvents.next({id, amount, type}));
    return control;
  }
}
