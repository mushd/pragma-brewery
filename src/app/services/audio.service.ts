import { Service } from 'typedi';
import { Observable } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';

/**
 * Audio Service
 */
@Service()
export class AudioService {

  private _beep: HTMLAudioElement;

  /**
   * Plays and alarm sound
   */
  public playAlarm(): Observable<void> {
    return fromPromise(this.beep.play().catch());
  }

  /**
   * Creates and return a audio dom element
   */
  protected get beep(): HTMLAudioElement {
    if (!this._beep) {
      const beep = document.createElement('audio');
      beep.autoplay = false;
      beep.load();
      beep.src = 'https://www.soundjay.com/misc/censor-beep-01.mp3';
      this._beep = beep;
    }

    return this._beep;
  }
}
