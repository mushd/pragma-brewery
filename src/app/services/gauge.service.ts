import { Service } from 'typedi';
import { BeerContainer } from '@shared/models/beer-container';
import { RadialGauge } from 'canvas-gauges';
import { Observable, ReplaySubject } from 'rxjs';
import { TemperatureChange } from '@shared/models';

/**
 * Gauge Service
 */
@Service()
export class GaugeService {

  private _gauges = [];
  private _maxValue = 10;
  private _minValue = 0;
  private _colorNormal = '#BABAB2';
  private _colorAlert = '#D66464';
  private _alerts$: ReplaySubject<number> = new ReplaySubject();
  private _values: number[] = [];
  private _canvases: HTMLCanvasElement[] = [];

  /**
   * Returns the containers alerts subject
   * @return {Observable<number>} the alert subject.
   */
  temperatureAlerts(): Observable<number> {
    return this._alerts$;
  }

  /**
   * Creates a gauge for a type of beer
   * @param {BeerContainer} container - The type of beer.
   * @return {HTMLCanvasElement} the gauge canvas element.
   */
  createForBeer(container: BeerContainer): HTMLCanvasElement {

    const minValue = container.temperature.min;
    const maxValue = container.temperature.max;
    const value = container.temperature.current;
    const canvas = document.createElement('canvas');

    // For e2e testing attributes
    canvas.id = `container-gauge-${this._gauges.length + 1}`;
    canvas.className = 'container-gauge';
    canvas.setAttribute('data-name', container.beer);
    canvas.setAttribute('data-min', minValue.toString());
    canvas.setAttribute('data-max', maxValue.toString());
    canvas.setAttribute('data-value', value.toString());
    canvas.setAttribute('data-alert', 'off');

    // creates the gauge
    const gauge = new RadialGauge({
      renderTo: canvas,
      width: 160,
      height: 160,
      minorTicks: maxValue,
      majorTicks: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      maxValue: 10,
      value,
      units: container.beer,
      colorUnits: this._colorNormal,
      fontUnitsSize: 35,
      highlights: [
        {from: minValue, to: maxValue, color: this._colorAlert}
      ]
    }).draw();

    this._canvases.push(canvas);
    this._gauges.push(gauge);
    this._values.push(value);
    return canvas;
  }

  /**
   * Changes the containers of a gauge.
   * @param {TemperatureChange} change - The temperature to be changed.
   * @return {number} the gauge new amount.
   */
  changeTemperature(change: TemperatureChange): number {
    const index = change.id;
    const amount = change.amount;

    if (!this._gauges[index]) {
      return null;
    }

    const newAmount = this.calculateTemperatureThresholds(index, amount);

    if(this._gauges[index].value !== newAmount) {
      this.calculateAlert(index, newAmount);
      this._gauges[index].value = newAmount;
      this._values[index] = newAmount;
      this._canvases[index].setAttribute('data-value', newAmount.toString());
    }
    return newAmount;
  }

  /**
   * Calculates the containers threshold for a gauge.
   * @param {number} index - The index of the gauge to be changed.
   * @param {number} amount - The amount to be changed.
   * @return {number} the gauge new amount.
   */
  protected calculateTemperatureThresholds(index: number, amount: number): number {
    let newAmount = this._values[index] + amount;

    if (newAmount < this._minValue) {
      newAmount = this._minValue;
    }
    if (newAmount > this._maxValue) {
      newAmount = this._maxValue;
    }
    return newAmount;
  }

  /**
   * Calculates if a gauge should emit an alert.
   * @param {number} index - The index of the gauge to be changed.
   * @param {number} amount - The amount to be changed.
   */
  protected calculateAlert(index: number, amount: number): void {
    const gauge = this._gauges[index];
    const tempRange = this._gauges[index].options.highlights[0];

    if (amount < tempRange.from || amount > tempRange.to) {
      if (!this.isGaugeInAlertState(gauge)) {
        this._alerts$.next(index);
        gauge.options.colorValueBoxBackground = this._colorAlert;
        gauge.options.colorUnits = this._colorAlert;
        this._canvases[index].setAttribute('data-alert', 'on');
      }
    } else {
      gauge.options.colorValueBoxBackground = this._colorNormal;
      gauge.options.colorUnits = this._colorNormal;
      this._canvases[index].setAttribute('data-alert', 'off');
    }
  }

  /**
   * Checks if a gauge is in alert state.
   * @param {RadialGauge} gauge - The index of the gauge to be changed.
   * @return {boolean} true if alert state.
   */
  protected isGaugeInAlertState(gauge: RadialGauge): boolean {
    return gauge.options.colorValueBoxBackground === this._colorAlert;
  }
}
