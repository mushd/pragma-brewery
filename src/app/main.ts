import 'reflect-metadata';
import '@app/styles//main.scss';
import { PragmaBreweryModule } from './app.module';
import { Container } from 'typedi';

Container.get(PragmaBreweryModule);
