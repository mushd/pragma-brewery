import { map, tap } from 'rxjs/operators';
import { Service } from 'typedi';
import { GaugeService } from '@app/services/gauge.service';
import { AudioService } from '@app/services/audio.service';
import { ControlService } from '@app/services/control.service';
import { ContainerService } from '@app/services/api/container.service';
import { SocketClientService } from '@app/services/socket-client.service';
import { TemperatureService } from '@app/services/api/temperature.service';

@Service()
export class PragmaBreweryModule {

  constructor(
    protected containerService: ContainerService,
    protected gaugeService: GaugeService,
    protected audioService: AudioService,
    protected controlService: ControlService,
    protected temperatureService: TemperatureService,
    protected socketService: SocketClientService
  ) {
    socketService.start();
    this.initiate();
  }

  initiate(): void {

    this.containerService.list()
      .pipe(
        map((containers) => containers.map((beer, index) =>
          this.controlService.appendGauge(this.gaugeService.createForBeer(beer), index)))
      ).subscribe();

    this.socketService.changes()
      .pipe(tap((change) => this.gaugeService.changeTemperature(change)))
      .subscribe();

    this.gaugeService.temperatureAlerts()
      .pipe(tap(() => this.audioService.playAlarm()))
      .subscribe();

    this.controlService.controlEvents()
      .pipe(tap((change) =>
        this.temperatureService.changeTemperature(change)
      ))
      .subscribe();
  }
}
