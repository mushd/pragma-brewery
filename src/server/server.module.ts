import { Service, Container } from 'typedi';
import { useExpressServer, useContainer } from 'routing-controllers';
import { createServer, Server } from 'http';
import { SocketServerService } from '@server/services/socket-server.service';
import { ContainerController } from '@server/controllers/container.controller';
import { TemperatureController } from '@server/controllers/temperature.controller';
import { TemperatureGenerator } from '@shared/utils/temperature.generator';
import { switchMap, tap } from 'rxjs/operators';
import { ContainerRepository } from '@server/repositories/container.repository';
import { TemperatureChangeRepository } from '@server/repositories/temperature-change.repository';
import { fromPromise } from 'rxjs/internal-compatibility';
import { TemperatureChange } from '@shared/models';
import path from 'path';
import express from 'express';

@Service()
export class ServerModule {

  private _app: express.Application;
  private _http: Server;

  constructor(
    protected temperatureService: TemperatureGenerator,
    protected socketService: SocketServerService,
    protected containerRepository: ContainerRepository,
    protected temperatureRepository: TemperatureChangeRepository
  ) {}

  async start(port: number): Promise<void> {
    this._app = express();
    this._http = createServer(this._app);
    this._app.use('/', express.static(path.join(__dirname, 'app')));
    this.socketService.start(this._http);
    this.registerControllers();
    this.listen(port);
    await this.listenTemperature();
  }

  registerControllers(): void {

    useContainer(Container);
    useExpressServer(this._app, {
      cors: true,
      validation: false,
      controllers: [
        ContainerController,
        TemperatureController
      ]
    });
  }

  private listen(port: number): void {
    this._http.listen(port, () => console.log(`Server listening on ${port}`));
  }

  private async listenTemperature(): Promise<void> {
    const beers = await this.containerRepository.list();
    this.temperatureService.temperatureChanges(beers.length)
      .pipe(
        switchMap(change => fromPromise(this.temperatureRepository.create(change))),
        tap((change: TemperatureChange) => this.socketService.emitChange(change))
      )
      .subscribe();
  }
}
