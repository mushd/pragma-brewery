import 'reflect-metadata';
import { ServerModule } from './server.module';
import { Container } from 'typedi';

const PORT = process.env.PORT || 4300;

const server = Container.get(ServerModule);

server.start(PORT as number)
  .catch((error) => console.log(error));
