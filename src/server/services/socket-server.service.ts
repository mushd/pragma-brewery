import { Service } from 'typedi';
import { Server } from 'http';
import { TemperatureChange } from '@shared/models';
import { SocketMessages } from '@shared/enums/socket-messages.enum';
import socket from 'socket.io';

@Service()
export class SocketServerService {

  private _server: socket.Server;

  start(http: Server): void {
    this._server = socket(http);

    this._server.on(SocketMessages.Connection, () =>{
      console.log('client connected');
    });
  }

  emitChange(change: TemperatureChange): void {
    this._server.emit(SocketMessages.TemperatureChange, JSON.stringify(change));
  }

  emitAlert(): void {
    this._server.emit(SocketMessages.Alert);
  }
}
