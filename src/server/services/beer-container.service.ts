import { TemperatureChange } from '@shared/models';
import { ContainerRepository } from '@server/repositories/container.repository';
import { TemperatureChangeRepository } from '@server/repositories/temperature-change.repository';
import { SocketServerService } from '@server/services/socket-server.service';
import { Service } from 'typedi';

@Service()
export class BeerContainerService {

  constructor(
    protected containerRepository: ContainerRepository,
    protected temperatureRepository: TemperatureChangeRepository,
    protected socketServerServer: SocketServerService
  ) {
  }

    async changeTemperature(id: number, change: TemperatureChange) {

      // get the existent container
      const beerContainer = await this.containerRepository.get(id);

      if (beerContainer === null)
        return Promise.reject();

      // update the container temperature
      const newAmount = beerContainer.temperature.current + change.amount;
      beerContainer.temperature.current  = newAmount;
      const updateContainer = await this.containerRepository.update(id, beerContainer);

      if(updateContainer === null)
        return Promise.reject('');

      // create temperature change history
      const createChange = await this.temperatureRepository.create(change);

      if(createChange === null)
        return Promise.reject('');

      // emits a socket change event
      this.socketServerServer.emitChange(createChange);

      if(beerContainer.temperature.min < newAmount || newAmount > beerContainer.temperature.max)
        this.socketServerServer.emitAlert();

      return Promise.resolve(beerContainer);
  }
}
