import { JsonController, Param, BadRequestError, Get, Put, Body } from 'routing-controllers';
import { TemperatureChangeRepository } from '@server/repositories/temperature-change.repository';
import { TemperatureChange } from '@shared/models';
import { BeerContainerService } from '@server/services/beer-container.service';

@JsonController('/temperature')
export class TemperatureController {

  constructor(
    protected temperatureRepository: TemperatureChangeRepository,
    protected beerContainerService: BeerContainerService
  ) {
  }

  @Get()
  list() {
    return this.temperatureRepository.list();
  }

  @Get('/:id')
  get(@Param("id") id: number) {

    if (id === undefined)
      return new BadRequestError('invalid');

    return this.temperatureRepository.get(id);
  }

  @Put('/:id')
  put(@Param("id") id, @Body() change: TemperatureChange) {

    if (id === undefined || change === undefined)
      throw new BadRequestError('invalid');

    return this.beerContainerService.changeTemperature(id, change);

  }
}
