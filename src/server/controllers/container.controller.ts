import { JsonController, Param, Get, BadRequestError } from 'routing-controllers';
import { ContainerRepository } from '@server/repositories/container.repository';
import { SocketServerService } from '@server/services/socket-server.service';

@JsonController('/containers')
export class ContainerController {

  constructor(
    protected containerRepository: ContainerRepository,
    protected socketServerServer: SocketServerService
  ) {
  }

  @Get()
  list() {
    return this.containerRepository.list();
  }

  @Get('/:id')
  get(@Param("id") id: number) {

    if (id === undefined)
      return new BadRequestError('invalid');

    return this.containerRepository.get(id);
  }
}
