import { Service } from 'typedi';
import { BeerContainer } from '@shared/models';
import { BaseRepository } from '@shared/utils/base.repository';
import { loadBeerData } from '@shared/utils/load-beer-data';

@Service()
export class ContainerRepository extends BaseRepository<BeerContainer> {

  constructor() {
    super(loadBeerData());
  }
}
