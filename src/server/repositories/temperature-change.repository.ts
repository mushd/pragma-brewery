import { BaseRepository } from '@shared/utils/base.repository';
import { TemperatureChange } from '@shared/models';

export class TemperatureChangeRepository extends BaseRepository<TemperatureChange> {

  constructor() {
    super(new Map());
  }
}
