export enum SocketMessages {
  Connection = 'connection',
  TemperatureChange = 'change',
  Alert = 'alert'
}
