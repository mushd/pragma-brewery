import { BeerContainer } from '@shared/models/beer-container';

export function loadBeerData() {

  const map: Map<number, BeerContainer> = new Map();
  const beerData = [
    {
      "name": "Pilsner",
      "min": 4,
      "max": 6
    },
    {
      "name": "IPA",
      "min": 5,
      "max": 6
    },
    {
      "name": "Lager",
      "min": 4,
      "max": 7
    },
    {
      "name": "Stout",
      "min": 6,
      "max": 8
    },
    {
      "name": "Wheat beer",
      "min": 3,
      "max": 5
    },
    {
      "name": "Pale Ale",
      "min": 4,
      "max": 6
    }
  ];

  beerData.forEach((beer, index) => map.set(index, {
    id: index,
    beer: beer.name,
    temperature: {
      min: beer.min,
      max: beer.max,
      current: Math.floor((beer.min + beer.max) / 2)
    }
  }));

  return map;
}
