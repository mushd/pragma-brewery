import { fromPromise } from 'rxjs/internal-compatibility';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import axios from  'axios';

export abstract class BaseApiService<T> {

  protected _api: string = __API__;

  protected constructor(protected _uri: string) {}

  public get(id: number): Observable<T> {
    return fromPromise(axios.get(this.resolveUrl(id)))
      .pipe(map(response => response.data));
  }

  public list(): Observable<T[]> {
    return fromPromise(axios.get(this.resolveUrl()))
      .pipe(map(response => response.data));
  }

  public post(data?: T): Observable<T> {
    return fromPromise(axios.post(this.resolveUrl(), data));
  }

  public put(id: number, data?: T): Observable<T> {
    return fromPromise(axios.put(this.resolveUrl(id), data));
  }

  public resolveUrl(url: number | string = ''): string {
    return `${this._api}/${this._uri}/${url}`;
  }
}
