import { BaseModel } from '@shared/models/base.model';

export abstract class BaseRepository<T = BaseModel> {

  protected constructor(protected _data: Map<number, T>) {
    if(_data === null) {
      throw new Error('Invalid data');
    }
  }

  get(id: number): Promise<T> {
    return Promise.resolve(this._data.get(id));
  }

  list(): Promise<T[]> {
    return Promise.resolve(Array.from(this._data.values()));
  }

  async create?(data: T): Promise<T> {
    const id = this._data.size + 1;
    const item: T = {id, ...data};
    this._data.set(id, item).get(id);
    return Promise.resolve(item);
  }

  async update?(id: number, model: T): Promise<T> {
    const update = await this.get(id);
    return new Promise((resolve, reject) => {
      if (update === null) {
        reject('not found');
      }

      const updated = {...update, ...model};
      this._data[id] = {...update, ...model};
      resolve(updated);
    });
  }

  async delete?(id: number): Promise<void> {
    const toDelete = await this.get(id);
    return new Promise((resolve, reject) => {

      if (toDelete === null) {
        reject('not found');
      }

      this._data.delete(id);
    });
  };
}
