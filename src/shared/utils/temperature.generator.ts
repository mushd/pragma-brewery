import { Service } from 'typedi';
import { Observable, Observer, of, timer } from 'rxjs';
import { tap, repeat, switchMap } from 'rxjs/operators';
import { TemperatureChange } from '@shared/models/temperature-change.model';
import { ChangeTypes } from '@shared/enums/change-type.enum';

/**
 * Temperature Service
 */
@Service()
export class TemperatureGenerator {

  constructor() {
  }

  /**
   * Returns an observable for containers changes
   * @param {number} noOfContainers - The number of containers to control.
   * @return {Observable<TemperatureChange>} the gauge new amount.
   */
  temperatureChanges(noOfContainers: number = 1): Observable<TemperatureChange> {
    return new Observable((observer: Observer<TemperatureChange>) => {
      this.generateRandomChanges(observer, noOfContainers);
    });
  }

  /**
   * Generates random containers change.
   * @param {Observer<TemperatureChange>} observer.
   * @param {number} noOfContainers - The number of containers to control.
   * @return {Observable<TemperatureChange>} the gauge new amount.
   */
  private generateRandomChanges(observer: Observer<TemperatureChange>, noOfContainers: number) {
    of({})
      .pipe(
        switchMap(() => this.generateRandomTimer()),
        tap(() => observer.next(this.generateRandomTemperatureChange(noOfContainers))),
        repeat()
      ).subscribe();
  }

  /**
   * Generates a random timer observable.
   * @return {Observable<number>} the timer observable.
   */
  private generateRandomTimer(): Observable<number> {
    return timer(this.generateRandomBetween(1, 5) * 1000);
  }

  /**
   * Generates a random number between range.
   * @param {number} from - The range from.
   * @param {number} to - The range to.
   * @return {number} the random number.
   */
  private generateRandomBetween(from: number, to: number): number {
    return (Math.floor(Math.random() * (to - from + 1)) + from);
  }

  /**
   * Generates a random containers change.
   * @param {number} noOfContainers - The number of containers to control.
   * @return {TemperatureChange} the containers change.
   */
  private generateRandomTemperatureChange(noOfContainers: number): TemperatureChange {
    const type = ChangeTypes.Other;
    return {
      id: this.generateRandomBetween(0, noOfContainers - 1),
      amount: this.generateRandomBetween(-1, 1),
      type
    };
  }
}
