/**
 * Temperature Model
 */
export interface Temperature {
  min: number;
  max: number;
  current?: number;
}
