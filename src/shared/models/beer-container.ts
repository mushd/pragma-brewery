import { Temperature } from './temperature.model';

/**
 * BeerContainer Model
 */
export interface BeerContainer {
  id: number;
  beer: string;
  temperature: Temperature;
}
