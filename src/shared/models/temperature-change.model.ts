import { ChangeTypes } from '@shared/enums/change-type.enum';

/**
 * TemperatureChange Model
 */
export interface TemperatureChange {
  id: number;
  amount: number;
  type: ChangeTypes;
}
