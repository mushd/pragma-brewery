## Pragma Brewery Code Test

#### Installation

`git clone clone https://bitbucket.org/mushd/pragma-brewery`

`cd pragma-brewery`

`npm install`
#### Serve Application
`npm start`
 
 front-end `http://localhost:4200`
 
 back-end `http://localhost:4200`
#### Test
All tests

`npm run test`

Unit tests

`npm run test:unit`

e2e tests

`npm run test:e2e`

#### Build
`npm run build`

### Assumptions

- A container stores only one type o beer at time.
- The container is initialized with the average temperature of a beer: ` min + max / 2`

### Improvements

- Make use of a single page application framework, eg. Angular, React, Vuejs
- Manage state with Redux pattern. 
- Push Notifications System with container statuses.
- Implement temperature precision increase, eg. 4.6°C.
